---
layout: null
---
# About LICENSING

It is primordial when it comes to cryptography that the user
can audit the code and acquire trust in the system.
Therefore all code linked to security and authentication MUST
be opensource for anyone to review
(security by offuscation is not an option).


All code is separated in 3 categories :

 - application specific code : according NFTpress Licensing choice
 - "security" specific code and libraries : Open Source ([GPLv3][GPL3])
 - the rest of the code is [MIT licensed](https://mit-license.org/) unless otherwise specified.


Agreed on {{page.date}} by

 - Doctor I·T
 - NFTpress


[GPL3]: gpl-3.0-standalone.html
